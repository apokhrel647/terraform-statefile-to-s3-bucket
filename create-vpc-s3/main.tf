variable "region" {
    default = "us-east-1"
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.region
}

terraform {
  backend "s3" {
    bucket         = "terraform-state-ap1979"
    key            = "networking/vpc1/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "ap-dynamodb-lock-state"
  }
}

resource "aws_vpc" "vpc1" {
  cidr_block = "10.0.0.0/16"
  tags = {
    "Name" = "vpc1"
  }  
}