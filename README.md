# terraform-statefile-to-s3-bucket
***************************************************
First create the S3 Bucket (terraform-state-ap1979) where we can store our Terraform state files (Be in: create-s3-dynamo)
***************************************************
Create Dynamo DB (ap-dynamodb-lock-state) which locks the State file if it's in use
***************************************************
Create the sample VPC and send the State file to the newly created S3 Bucket (Be in: create-vpc-s3)
***************************************************
Browse your state file under the S3 Bucket: networking/vpc1/terraform.tfstate
***************************************************

***************************************************
Note: Make sure you have configured your AWS credentials to run these scripts.